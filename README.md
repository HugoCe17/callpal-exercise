# Callpal Exercise

## To Deploy
1. `git clone https://HugoCe17@bitbucket.org/HugoCe17/callpal-exercise.git`
2. `cd callpal-exercise`
3. `npm install`
4. run `mongod` in another Terminal session
5. Switch back to `callpal-exercise` directory
6. `npm start`
7. open `localhost:3000/`
8. Load Dummy Data on Click 
9. The API is at `localhost:3000/api`
10. Finally, Users can be retrieved at `localhost:3000/api/users` upon successful login

## Basic Auth
username: "admin"
password: "password"

## Using Mongo to check DB
1. You can invoke 'mongo' then,
2. invoke `use callpal_users` 
3. and finally invoke `db.users.find().pretty()` to fetch all data in the DB

 
