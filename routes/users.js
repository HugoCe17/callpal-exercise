var express = require('express');
var router = express.Router();
/** Models */
var User = require('../models/user');
var basicAuth = require('basic-auth');


/** an auth helper function */
var auth = function (req, res, next) {
  function unauthorized(res) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    return res.sendStatus(401);
  };

  var user = basicAuth(req);

  if (!user || !user.name || !user.pass) {
    return unauthorized(res);
  };

  /** grant foo access for testing purposes */
  if (user.name === 'admin' && user.pass === 'password') {
    return next();
  } else {
    return unauthorized(res);
  };
};

/** "/"
 *  GET: API index
 * 
 */
router.get('/', function (req, res, next) {
  return res.json({
    message: 'Welcome to the face of this Awesome API!',
    more: 'Data has been loaded! check ',
    someMore: 'You will be kindly asked to login if you wish to see whats behind the "/api/users" endpoint',
  })
})

/** "/users"
 *  GET: finds all users
 *  POST: creates a new User
 */
router.get('/users', auth, function (req, res, next) {
  User.find({}, function (err, users) {
    if (err) return res.send(err);

    return res.json(users)
  })
});

router.post('/users', function (req, res) {

  var user = new User({
    email: req.body.email,
    name: req.body.name,
    password: req.body.password,
    country: req.body.country,
    city: req.body.city,
    state: req.body.state,
  })

  user.save(function (err) {
    if (err) return res.send(err);

    return res.json({ message: 'User Created' })
  })

});

/** "/users/:user_id"
 *  GET: find user by id
 *  PUT: update user by id
 *  DELETE: destrorys user by id
 */
router.get('/users/:user_id', function (req, res) {
  User.findById(req.params.user_id, function (err, user) {
    if (err) return res.send(err);
    return res.json(user)
  })
});

router.put('/users/:user_id', auth, function (req, res) {
  User.findById(req.params.user_id, function (err, user) {
    if (err) return res.send(err);

    user.email = req.body.email;
    user.name = req.body.name;
    user.password = req.body.password;
    user.country = req.body.country;
    user.city = req.body.city;
    user.state = req.body.state;

    user.save(function (err) {
      if (err) res.send(err)
    })

    return res.json({ message: 'User updated!' })

  })
});

router.delete('/users/:user_id', auth, function (req, res) {
  User.remove({
    _id: req.params.user_id
  }, function (err, user) {
    if (err) return res.send(err);

    return res.json({ message: 'Successfully deleted' });
  })
})

module.exports = router;
