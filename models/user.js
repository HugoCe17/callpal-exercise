var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// var passportLocalMongoose = require('passport-local-mongoose');

// create a schema
var userSchema = new Schema({
	email: { type: String, required: true, unique: true },
	name: String,
	country: String,
	city: String,
	state: String,
});

/** adds { password: String } to schema */
userSchema.plugin(require('mongoose-bcrypt'));

// userSchema.plugin(passportLocalMongoose);


module.exports = mongoose.model('User', userSchema);