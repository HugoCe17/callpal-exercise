(function (event) {
  event(jQuery, document, window)
} (function ($, document, window) {
  $(function () {
    var $message = $('.message');
    var $primaryBtn = $('#load-users');

    $primaryBtn.on('click', function () {
      self = this;
      $.ajax({
        url: 'http://localhost:3000/api/users',
        method: 'GET',
        crossDomain: true,
        success: function (data, txtStatus, xhr) {
          console.log(data);

          /** Disable button if DB has Users */
          if (data.length !== 0) {
            $(self).prop('disabled', true);

            /** Show message once button is disabled */
            if ($message.hasClass('hidden')) {
              $message.removeClass('hidden')
            }

            /** populate DB with Dummy Data */
            loadDummyData()

            console.log('Empty')
          }
          console.log(xhr.status);
        }
      });

      function loadDummyData() {
        var dummyData = [{
          email: 'HugoCe17@gmail.com',
          name: 'Hugo',
          password: 'toBeEncripted',
          country: 'USA',
          city: 'Miami',
          state: 'FL',
        },
          {
            email: 'Aeriel@gmail.com',
            name: 'Aeriel',
            password: 'toBeEncripted',
            country: 'USA',
            city: 'Cooper City',
            state: 'FL',
          },
          {
            email: 'Arod@gmail.com',
            name: 'Alex',
            password: 'toBeEncripted',
            country: 'USA',
            city: 'Miami',
            state: 'FL',
          }]

        return dummyData.map(function (user, index, arr) {
          console.log(user)
          $.post('/api/users', user)
            .done(function (data) {
              console.log('data successfully loaded')
            });
        });
      }

    });
  });
}));